package com.example.samte.thexprojectandroid;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgottenPassword extends AppCompatActivity implements View.OnClickListener{

    private EditText txtUserName;
    private Button btnSend;
    private ProgressBar progressBar;
    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotten_password);

        txtUserName = findViewById(R.id.txt_username_fp);
        btnSend = findViewById(R.id.btn_send_fp);
        progressBar = findViewById(R.id.progresbar_fp);
        progressBar.setVisibility(View.GONE);
        firebaseAuth = FirebaseAuth.getInstance();

        btnSend.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        changePassword();
    }

    private void changePassword(){
        String email = txtUserName.getText().toString().trim();

        if (TextUtils.isEmpty(email)){
            Toast.makeText(getApplicationContext(), "Enter your email adress", Toast.LENGTH_SHORT).show();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        firebaseAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            finish();
                            startActivity(new Intent(ForgottenPassword.this, LoginPage.class));
                            Toast.makeText(ForgottenPassword.this, "We have sent you instructions to reset your password!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(ForgottenPassword.this, "Failed to send reset email!", Toast.LENGTH_SHORT).show();
                        }

                        progressBar.setVisibility(View.GONE);

                    }
                });
    }
}

package com.example.samte.thexprojectandroid.Shopping_Cart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.samte.thexprojectandroid.R;

/**
 * Created by Berkay on 6.01.2018.
 */

public class ShoppingHistory extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shopping_history_fragment,container,false);
        return view;
    }
}

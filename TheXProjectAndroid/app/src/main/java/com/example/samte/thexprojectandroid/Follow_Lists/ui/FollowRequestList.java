package com.example.samte.thexprojectandroid.Follow_Lists.ui;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.samte.thexprojectandroid.Follow_Lists.adapter.UserAdapter;
import com.example.samte.thexprojectandroid.Follow_Lists.model.ListItem;
import com.example.samte.thexprojectandroid.Follow_Lists.model.UserData;
import com.example.samte.thexprojectandroid.R;

import java.util.ArrayList;

public class FollowRequestList extends Fragment implements UserAdapter.ItemClickCallback{
    private static final String BUNDLE_EXTRAS = "BUNDLE_EXTRAS";
    private static final String EXTRA_QUOTE = "EXTRA_QUOTE";
    private static final String EXTRA_ATTR = "EXTRA_ATTR";


    private RecyclerView recView;
    private UserAdapter adapter;
    private ArrayList listData;
    /*private Toolbar toolbar;
    private TextView toolbar_text;*/



    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_request_list);

        toolbar = findViewById(R.id.toolbar_top_follow);
        toolbar_text = findViewById(R.id.toolbar_top_follow_title);
        setSupportActionBar(toolbar);
        toolbar_text.setText("Follow Requests");


        getSupportActionBar().setDisplayShowTitleEnabled(false);

        listData = (ArrayList) UserData.getListData();

        recView = findViewById(R.id.rec_list);
        //GridLayoutManager also usable
        //StaggeredGridLayoutManager also usable
        recView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new UserAdapter(listData, this);
        recView.setAdapter(adapter);
        adapter.setItemClickCallback(this);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(createHelperCallback());
        itemTouchHelper.attachToRecyclerView(recView);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_follow_request_list,container,false);
        listData = (ArrayList) UserData.getListData();

        recView = view.findViewById(R.id.rec_list);

        recView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new UserAdapter(listData, getActivity());
        recView.setAdapter(adapter);
        adapter.setItemClickCallback(this);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(createHelperCallback());
        itemTouchHelper.attachToRecyclerView(recView);
        return view;
    }

    /*//navigation bar için
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //mTextMessage.setText("Home");
                    return true;
                case R.id.navigation_cart:
                    //mTextMessage.setText("Cart");
                    return true;
                case R.id.navigation_filter:
                    //mTextMessage.setText("Filter");
                    return true;
                case R.id.navigation_star:
                    //mTextMessage.setText("Favourities");
                    return true;
            }

            return false;
        }

    };*/


    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                        ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                        return true;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        deleteItem(viewHolder.getAdapterPosition());
                    }
                };
        return simpleItemTouchCallback;
    }




    private void moveItem(int oldPos, int newPos) {
        ListItem item = (ListItem) listData.get(oldPos);
        listData.remove(oldPos);
        listData.add(newPos, item);
        adapter.notifyItemMoved(oldPos, newPos);
    }

    private void deleteItem(int position) {
        listData.remove(position);
        adapter.notifyItemRemoved(position);
    }

    @Override
    public void onItemClick(int p) {
        /*ListItem item = (ListItem) listData.get(p);

        Intent i = new Intent(this, FriendProfile.class);

        Bundle extras = new Bundle();
        extras.putString(EXTRA_QUOTE, item.getTitle());
        extras.putString(EXTRA_ATTR, item.getSubTitle());

        i.putExtra(BUNDLE_EXTRAS, extras);
        startActivity(i);*/

    }

    @Override
    public void onSecondaryItemClick(int p) {
        ListItem item = (ListItem) listData.get(p);

        if (item.isFavourite()){
            item.setFavourite(false);
        }else {
            item.setFavourite(true);
        }

        adapter.notifyDataSetChanged();
    }
}

package com.example.samte.thexprojectandroid;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewStub;
import android.widget.GridView;

import com.example.samte.thexprojectandroid.Profile.*;
import com.example.samte.thexprojectandroid.Profile.Product;

import java.util.ArrayList;
import java.util.List;


public class ProfilePage extends Fragment {

    private GridView gridView;
    private ProfileAdapter profileAdapter;
    private ViewStub viewStub;
    private List<com.example.samte.thexprojectandroid.Profile.Product> productList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.profilepage_activity, container, false);

        gridView = view.findViewById(R.id.cont_grid);

        viewStub = view.findViewById(R.id.profileViewStub);
        viewStub.inflate();

        profileAdapter = new ProfileAdapter(getContext(), R.layout.brandslist_singleitem, getProductList());

        gridView.setAdapter(profileAdapter);

        return view;
    }

    private List<Product> getProductList(){
        productList = new ArrayList<>();
        Product sample = new Product(R.drawable.mshoes_real);
        for (int i = 0; i < 20; i++) {
            productList.add(sample);
        }
        return productList;
    }


}

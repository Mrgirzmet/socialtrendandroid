package com.example.samte.thexprojectandroid.Shopping_Cart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.samte.thexprojectandroid.R;

/**
 * Created by Berkay on 6.01.2018.
 */

public class ShoppingAuthentication extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shopping_authentication_fragment,container,false);
        Button purchaseButton = view.findViewById(R.id.buttonpurchase);
        purchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getContext(),"Purchase is Complete",Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        return view;
    }
}

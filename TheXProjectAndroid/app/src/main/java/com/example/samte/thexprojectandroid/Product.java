package com.example.samte.thexprojectandroid;

/**
 * Created by Berkay on 4.01.2018.
 */

public class Product {
    private int ImageID;

    public Product(int imageID) {
        ImageID = imageID;
    }

    public int getImageID() {
        return ImageID;
    }

    public void setImageID(int imageID) {
        ImageID = imageID;
    }
}

package com.example.samte.thexprojectandroid.Brands_Clothes_Selection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.samte.thexprojectandroid.R;

/**
 * Created by Berkay on 5.01.2018.
 */

public class Brands_Clothes_Selection extends Fragment {
    ViewPager viewPager;
    Brands3Adapter adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.brands3_activity,container,false);
        viewPager = v.findViewById(R.id.brand3pager);
        adapter = new Brands3Adapter(getContext());
        viewPager.setAdapter(adapter);
        return v;
    }
}

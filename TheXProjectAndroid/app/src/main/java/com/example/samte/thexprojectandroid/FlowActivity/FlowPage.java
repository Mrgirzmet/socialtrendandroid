package com.example.samte.thexprojectandroid.FlowActivity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.samte.thexprojectandroid.R;

import java.util.ArrayList;

public class FlowPage extends Fragment implements FlowPageAdaptor.Communicator{
    private ArrayList<UserInfo> usersList = new ArrayList<UserInfo>();
    private RecyclerView recyclerView;
    private FlowPageAdaptor flowPageAdaptor;
    FollowPageCommunicator flowPageCallback;

    @Override
    public void productPictureClickListener(UserInfo userRelatedInformation) {
        flowPageCallback.requestingFullScreen(userRelatedInformation);
    }

    public interface FollowPageCommunicator
    {
        void requestingFullScreen(UserInfo userRelatedInformation);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            flowPageCallback = (FollowPageCommunicator) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_flow_page,container,false);
        recyclerView = view.findViewById(R.id.flowPageRecyclerView);
        flowPageAdaptor = new FlowPageAdaptor(usersList,this,recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(flowPageAdaptor);
        prepareUserData();
        return view;
    }
    private void prepareUserData() {

        UserInfo userInfo = new UserInfo("Mert",R.drawable.wskirt_real);
        usersList.add(userInfo);

        userInfo = new UserInfo("Berkay", R.drawable.mpants_real);

        usersList.add(userInfo);

        userInfo = new UserInfo("Omer", R.drawable.mshirt_real);
        usersList.add(userInfo);

        userInfo = new UserInfo("Cengiz", R.drawable.mshoes_real);
        usersList.add(userInfo);

        userInfo = new UserInfo("Sinan", R.drawable.mwatch_real);
        usersList.add(userInfo);

        userInfo = new UserInfo("OmerBerk", R.drawable.mwatch_real);
        usersList.add(userInfo);

        userInfo = new UserInfo("Mert",R.drawable.wskirt_real);
        usersList.add(userInfo);

        userInfo = new UserInfo("Berkay", R.drawable.mpants_real);

        usersList.add(userInfo);

        userInfo = new UserInfo("Omer", R.drawable.mshirt_real);
        usersList.add(userInfo);

        userInfo = new UserInfo("Cengiz", R.drawable.mshoes_real);
        usersList.add(userInfo);

        userInfo = new UserInfo("Sinan", R.drawable.mwatch_real);
        usersList.add(userInfo);
        //there is an error with related wbag_real
        userInfo = new UserInfo("OmerBerk", R.drawable.mwatch_real);
        usersList.add(userInfo);

        flowPageAdaptor.notifyDataSetChanged();
    }
}

package com.example.samte.thexprojectandroid.Favorites;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;

import com.example.samte.thexprojectandroid.R;

/**
 * Created by Berkay on 6.01.2018.
 */

public class FavoritesMain extends Fragment {

    private ViewStub stubSuggestions;
    private ViewStub stubFavorites;
    private ViewStub stubYourStuff;
    private Button btnSuggestions;
    private Button btnYourStuff;
    private int currentViewMode = 0;

    static final int VIEW_MODE_FAVORITES = 0;
    static final int VIEW_MODE_SUGGESTIONS = 1;
    static final int VIEW_MODE_YOURSTUFF = 2;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.favorites_activity,container,false);
        Toolbar toolbar = v.findViewById(R.id.toolbar);


        stubFavorites = v.findViewById(R.id.stub_favorites);
        stubSuggestions = v.findViewById(R.id.stub_suggestions);
        stubYourStuff = v.findViewById(R.id.stub_yourstuff);
        stubFavorites.inflate();
        stubSuggestions.inflate();
        stubYourStuff.inflate();

        btnSuggestions = v.findViewById(R.id.buttonSuggestions);
        btnSuggestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentViewMode +=1;
                stubFavorites.setVisibility(View.GONE);
                stubYourStuff.setVisibility(View.GONE);
                stubSuggestions.setVisibility(View.VISIBLE);
            }
        });
        btnYourStuff = v.findViewById(R.id.buttonYourStuff);
        btnYourStuff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentViewMode +=1;
                stubSuggestions.setVisibility(View.GONE);
                stubFavorites.setVisibility(View.GONE);
                stubYourStuff.setVisibility(View.VISIBLE);
            }
        });

        stubYourStuff.setVisibility(View.GONE);
        stubSuggestions.setVisibility(View.GONE);
        stubFavorites.setVisibility(View.VISIBLE);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("ViewMode",getActivity().MODE_PRIVATE);
        currentViewMode = sharedPreferences.getInt("currentViewMode",VIEW_MODE_FAVORITES);

        return v;
    }


}

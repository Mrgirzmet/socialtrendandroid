package com.example.samte.thexprojectandroid.Follow_Lists.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.samte.thexprojectandroid.Follow_Lists.model.ListItem;
import com.example.samte.thexprojectandroid.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OmerCeylan on 8.01.2018.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserHolder>{

    private List<ListItem> listData;
    private LayoutInflater inflater;

    private ItemClickCallback itemClickCallback;

    public interface ItemClickCallback{
        void onItemClick(int p);
        void onSecondaryItemClick(int p);
    }

    public void setItemClickCallback (final ItemClickCallback itemClickCallback){
        this.itemClickCallback = itemClickCallback;
    }

    public UserAdapter(List<ListItem> listData, Context c){
        this.inflater = LayoutInflater.from(c);
        this.listData = listData;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.follow_list_item, parent, false);

        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        ListItem item = listData.get(position);
        holder.title.setText(item.getTitle());
        holder.subTitle.setText(item.getSubTitle());
        if (item.isFavourite()){
            holder.secondary_icon.setImageResource(R.drawable.ic_close);
        }else{
            holder.secondary_icon.setImageResource(R.drawable.ic_done);
        }
    }

    public void setListData(ArrayList<ListItem> itemList){
        this.listData.clear();
        this.listData.addAll(itemList);
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }

    class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView title;
        private TextView subTitle;

        private ImageView thumbnail;
        private ImageView secondary_icon;

        private View container;

        public UserHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.lbl_item_text);
            subTitle = itemView.findViewById(R.id.lbl_item_sub_title);
            thumbnail = itemView.findViewById(R.id.im_item_icon);
            secondary_icon = itemView.findViewById(R.id.im_item_icon_secondary);
            secondary_icon.setOnClickListener(this);
            container = itemView.findViewById(R.id.cont_item_root);
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.cont_item_root){
                itemClickCallback.onItemClick(getAdapterPosition());
            }else {
                itemClickCallback.onSecondaryItemClick(getAdapterPosition());
            }
        }
    }
}

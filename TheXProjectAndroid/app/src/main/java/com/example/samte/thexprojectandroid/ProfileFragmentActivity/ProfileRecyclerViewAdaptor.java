package com.example.samte.thexprojectandroid.ProfileFragmentActivity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.example.samte.thexprojectandroid.FlowActivity.UserInfo;
import com.example.samte.thexprojectandroid.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
/**
 * Created by samte on 29/01/2018.
 */
public class ProfileRecyclerViewAdaptor extends RecyclerView.Adapter<ProfileRecyclerViewAdaptor.MyViewHolder> {
    private final ArrayList<Integer> resources;
    private RecyclerView recyclerView;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageButton profileFavoriteImageButton;
        MyViewHolder(View view) {
            super(view);
            profileFavoriteImageButton =  view.findViewById(R.id.productImageButton);
        }
    }
//    interface Communicator
//    {
//        void favoriteProductPictureOnClickListener(Integer productResource);
//    }
    ProfileRecyclerViewAdaptor(ArrayList<Integer> resources, RecyclerView recyclerView) {
        this.resources = resources;
        this.recyclerView = recyclerView;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_fragment_recyclerview_item_row, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final int local_position = position;
        Integer UserRelatedInformation = resources.get(position);
        holder.profileFavoriteImageButton.setImageResource(UserRelatedInformation);
        holder.profileFavoriteImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                communicator.favoriteProductPictureOnClickListener(resources.get(local_position));
            }
        });
    }
    @Override
    public int getItemCount() {
        return resources.size();
    }
}

package com.example.samte.thexprojectandroid.FlowActivity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.samte.thexprojectandroid.R;

import java.util.ArrayList;
public class FlowPageAdaptor extends RecyclerView.Adapter<FlowPageAdaptor.MyViewHolder> {
    private RecyclerView recyclerView;
    private ArrayList<UserInfo> UserInformationList;
    private Communicator communicator;
    UserInfo myActiveInfo;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView username;
        ImageButton productPicture, tickButton, addButton, commentButton;
        MyViewHolder(View view) {
            super(view);
            username =  view.findViewById(R.id.username);
            productPicture = view.findViewById(R.id.productImageButton);
            addButton =  view.findViewById(R.id.addImageButton);
            tickButton = view.findViewById(R.id.tickImageButton);
            commentButton = view.findViewById(R.id.commentImageButton);
        }
    }
    interface Communicator
    {
        void productPictureClickListener(UserInfo userRelatedInformation);
    }
    FlowPageAdaptor(ArrayList<UserInfo> UserInformationList,Communicator communicator,RecyclerView recyclerView) {
        this.UserInformationList = UserInformationList;
        this.communicator = communicator;
        this.recyclerView = recyclerView;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.flowpage_recyclerview_item_row, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final int local_position = position;
        UserInfo UserRelatedInformation = UserInformationList.get(position);
        holder.username.setText(UserRelatedInformation.getUsername());
        holder.productPicture.setImageResource(UserRelatedInformation.getProductResourceInformation());
        holder.productPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        communicator.productPictureClickListener(UserInformationList.get(local_position));
            }
        });
    }
    @Override
    public int getItemCount() {
        return UserInformationList.size();
    }
}
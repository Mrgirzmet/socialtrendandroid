package com.example.samte.thexprojectandroid.Follow_Lists.model;

import com.example.samte.thexprojectandroid.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by OmerCeylan on 8.01.2018.
 */

public class UserData {

    private static final String[] titles = {"Ömer Ceylan", "Berkay Özdemir", "Mert Ünsal", "Onur Ceylan", "Burak Akten"};

    private static final String[] subTitles = {"İzmir University of Economics", "İzmir University of Economics", "İzmir University of Economics", "İzmir University of Economics", "İzmir University of Economics"};

    private static final int icon = R.drawable.ic_face;

    public static List<ListItem> getListData(){
        List<ListItem> data = new ArrayList<>();

        for (int x = 0; x < 5; x++) {
            for (int i = 0; i < titles.length; i++) {
                ListItem item = new ListItem();

                item.setTitle(titles[i]);
                item.setSubTitle(subTitles[i]);

                data.add(item);

            }
        }

        return data;
    }



}

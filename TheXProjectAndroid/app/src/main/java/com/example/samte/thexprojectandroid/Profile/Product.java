package com.example.samte.thexprojectandroid.Profile;

/**
 * Created by C505 on 9.01.2018.
 */

public class Product {

    private int imageId;

    public Product(int imageId) {
        this.imageId = imageId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}

package com.example.samte.thexprojectandroid.Brands_Clothes_Selection;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.samte.thexprojectandroid.R;

/**
 * Created by Berkay on 5.01.2018.
 */

public class Brands3Adapter extends PagerAdapter {
    private int[] image_resources = {R.drawable.wbag,R.drawable.mpants,R.drawable.mshirt,R.drawable.wskirt,R.drawable.wshoes};
    private String[] title_resources = {"ACCESSORIES","PANTS","SHIRTS","SKIRTS","SHOES"};
    private Context ctx;
    private LayoutInflater layoutInflater;

    public Brands3Adapter(Context ctx){
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return image_resources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (android.support.constraint.ConstraintLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view = layoutInflater.inflate(R.layout.brands3_options,container,false);
        ImageView imageView = (ImageView)item_view.findViewById(R.id.brands3_image);
        TextView textView = (TextView)item_view.findViewById(R.id.brands3_title);

        imageView.setImageResource(image_resources[position]);
        textView.setText(title_resources[position]);
        container.addView(item_view);

        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((android.support.constraint.ConstraintLayout)object);
    }
}

package com.example.samte.thexprojectandroid;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginPage extends AppCompatActivity implements Animation.AnimationListener{

    private ImageView mStLogoImageView;
    private EditText txtUserName;
    private EditText txtPassword;
    private Button btnLogin;
    private TextView lblCreateAccount;
    private TextView lblForgottenPassword;

    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this,ProgressDialog.STYLE_SPINNER);

        if(firebaseAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }

        mStLogoImageView = findViewById(R.id.logoWhiteImageView);
        txtUserName = findViewById(R.id.txt_email_login);
        txtPassword = findViewById(R.id.txt_password_login);
        btnLogin = findViewById(R.id.btnLogin);
        lblCreateAccount = findViewById(R.id.lbl_createAccount);
        lblForgottenPassword = findViewById(R.id.lbl_forgottenPassword);


        txtUserName.setVisibility(View.GONE);
        txtPassword.setVisibility(View.GONE);
        btnLogin.setVisibility(View.GONE);
        lblCreateAccount.setVisibility(View.GONE);
        lblForgottenPassword.setVisibility(View.GONE);


        Animation moveWhiteLogoAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_login_screen);
        moveWhiteLogoAnimation.setFillAfter(true);
        moveWhiteLogoAnimation.setAnimationListener(this);
        mStLogoImageView.startAnimation(moveWhiteLogoAnimation);

        lblCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginPage.this, CreatingAccount.class);
                startActivity(intent);
            }
        });

        lblForgottenPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginPage.this, ForgottenPassword.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userLogin();
            }
        });


    }

    private void userLogin(){
        //Reset Errors
        txtUserName.setError(null);
        txtPassword.setError(null);

        String email = txtUserName.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            txtPassword.setError(getString(R.string.error_invalid_password));
            focusView = txtPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            txtUserName.setError(getString(R.string.error_field_required));
            focusView = txtUserName;
            cancel = true;
        } else if (!isEmailValid(email)) {
            txtUserName.setError(getString(R.string.error_invalid_email));
            focusView = txtUserName;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            progressDialog.setMessage("Logging...");
            progressDialog.show();

            firebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {


                            if(task.isSuccessful()){
                                finish();
                                Intent intent = new Intent(LoginPage.this, MainActivity.class);
                                startActivity(intent);

                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(LoginPage.this, "Username or password is wrong please try again", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 8;
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        txtUserName.setAlpha(0f);
        txtUserName.setVisibility(View.VISIBLE);

        txtPassword.setAlpha(0f);
        txtPassword.setVisibility(View.VISIBLE);

        btnLogin.setAlpha(0f);
        btnLogin.setVisibility(View.VISIBLE);

        lblCreateAccount.setAlpha(0f);
        lblCreateAccount.setVisibility(View.VISIBLE);

        lblForgottenPassword.setAlpha(0f);
        lblForgottenPassword.setVisibility(View.VISIBLE);

        int mediumAnimationTime = getResources().getInteger(android.R.integer.config_mediumAnimTime);

        txtUserName.animate()
                .alpha(1f)
                .setDuration(mediumAnimationTime)
                .setListener(null);

        txtPassword.animate()
                .alpha(1f)
                .setDuration(mediumAnimationTime)
                .setListener(null);

        btnLogin.animate()
                .alpha(1f)
                .setDuration(mediumAnimationTime)
                .setListener(null);

        lblCreateAccount.animate()
                .alpha(1f)
                .setDuration(mediumAnimationTime)
                .setListener(null);

        lblForgottenPassword.animate()
                .alpha(1f)
                .setDuration(mediumAnimationTime)
                .setListener(null);

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


}

package com.example.samte.thexprojectandroid.ProfileFragmentActivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.example.samte.thexprojectandroid.R;
import java.util.ArrayList;
/**
 * Created by samte on 28/01/2018.
 */
public class ProfileFragment extends Fragment {
    ArrayList<Integer> profileFavoriteProductList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.profile_fragment_layout,container,false);
        TextView userName,userNickName,numberOfFollowers,numberOfFollowings;
        RecyclerView recyclerView;
        ImageButton profileImageButton;
        userName =  view.findViewById(R.id.userNameSurname);
        userNickName =  view.findViewById(R.id.userNickName);
        numberOfFollowings =  view.findViewById(R.id.numberOfFollowingTextView);
        numberOfFollowers =  view.findViewById(R.id.numberOfFollowersTextView);
        recyclerView = view.findViewById(R.id.profileFragmentRecyclerView);
        profileFavoriteProductList = new ArrayList<>();
        prepareUserData();
        ProfileRecyclerViewAdaptor profileRecyclerViewAdaptor = new ProfileRecyclerViewAdaptor(profileFavoriteProductList,recyclerView);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(profileRecyclerViewAdaptor);
        return view;
    }
    private void prepareUserData() {
        profileFavoriteProductList.add(R.drawable.wshoes_real);
        profileFavoriteProductList.add(R.drawable.wshoes_real);
        profileFavoriteProductList.add(R.drawable.wshoes_real);
        profileFavoriteProductList.add(R.drawable.wshoes_real);
    }


}

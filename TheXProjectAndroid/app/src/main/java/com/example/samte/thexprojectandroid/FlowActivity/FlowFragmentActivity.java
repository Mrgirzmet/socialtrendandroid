package com.example.samte.thexprojectandroid.FlowActivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.samte.thexprojectandroid.R;

/**
 * Created by samte on 24/01/2018.
 */

public class FlowFragmentActivity extends Fragment {
    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.flowpage_recyclerview_item_row_without_margin,container,false);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.setMargins(0, 0, 0, 0);
        view.requestLayout();
        TextView username;
        ImageButton productPicture, tickButton, addButton, commentButton;
        username =  view.findViewById(R.id.username);
        productPicture = view.findViewById(R.id.productImageButton);
        addButton =  view.findViewById(R.id.addImageButton);
        tickButton = view.findViewById(R.id.tickImageButton);
        commentButton = view.findViewById(R.id.commentImageButton);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            username.setText(bundle.getString("username"));
            productPicture.setImageResource(bundle.getInt("productImage"));
        }
        return view;
    }

}

package com.example.samte.thexprojectandroid;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.samte.thexprojectandroid.BrandsList.BrandsListActivity;
import com.example.samte.thexprojectandroid.Favorites.FavoritesMain;
import com.example.samte.thexprojectandroid.FlowActivity.FlowPage;
import com.example.samte.thexprojectandroid.Follow_Lists.ui.FollowRequestList;
import com.example.samte.thexprojectandroid.Shopping_Cart.ShoppingMain;

import java.util.ArrayList;

/**
 * Created by samtebirah on 14/12/2017.
 */

public class MainPage  extends AppCompatActivity implements DrawerLayoutMenuAdapter.ItemClickCallback {
    private DrawerLayout drawerLayout;
    private ListView listView;
    public static ArrayList<String> active_fragments_history = new ArrayList<String>();
    Context context;
    Fragment active_fragments;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context=this;
        active_fragments_history.add("mainpage");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainpage);
        //Typeface robotoLightTypeFace = Typeface.createFromAsset(getAssets(),"fonts/roboto_black.ttf");
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        listView = (ListView) findViewById(R.id.drawerList);
        listView.setBackground(ContextCompat.getDrawable(this, R.drawable.background_image));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");
        Button brand_button = (Button) findViewById(R.id.brandsButton);
        brand_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(active_fragments_history.get(active_fragments_history.size()-1).compareTo("BrandsList" ) != 0)
                {
                    drawerLayout.closeDrawer(GravityCompat.START);
                    active_fragments = new BrandsListActivity();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragmentContainer,active_fragments,"BrandsList");
                    fragmentTransaction.addToBackStack(null);
                    active_fragments_history.remove(active_fragments_history.size()-1);
                    active_fragments_history.add("BrandsList");
                    fragmentTransaction.commit();
                }
            }
        });


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.toolbar_bottom);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        DrawerLayoutMenuAdapter listViewAdapter = new DrawerLayoutMenuAdapter(this);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(Color.BLACK);
        ImageButton filter = findViewById(R.id.filter);
        filter.setVisibility(View.GONE);

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);
        listViewAdapter.setItemClickCallback(this);
        listViewAdapter.heightOfScreen = outMetrics.heightPixels;
        listView.setAdapter(listViewAdapter);

        active_fragments = new FlowPage();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer,active_fragments,"FlowPage");
        fragmentTransaction.addToBackStack("FlowPage");
        fragmentTransaction.commit();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(context,String.valueOf(i),Toast.LENGTH_SHORT).show();
                switch (i) {
                    case 0:
                    {
                        //ignore
                        break;
                    }
                    case 1:
                    {

                        //ignore
                        break;
                    }
                    case 2:
                    {
                        break;
                    }
                    case 3:
                    {
                        if(active_fragments_history.get(active_fragments_history.size()-1).compareTo("ShoppingMain" ) != 0) {
                            drawerLayout.closeDrawer(GravityCompat.START);
                            active_fragments = new ShoppingMain();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentContainer, active_fragments, "ShoppingMain");
                            fragmentTransaction.addToBackStack(null);
                            active_fragments_history.remove(active_fragments_history.size()-1);
                            active_fragments_history.add("ShoppingMain");
                            fragmentTransaction.commit();
                        }
                        break;
                    }
                    case 4:
                    {
                        if(active_fragments_history.get(active_fragments_history.size()-1).compareTo("FavoritesMain" ) != 0)
                        {
                            drawerLayout.closeDrawer(GravityCompat.START);
                            active_fragments = new FavoritesMain();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentContainer, active_fragments, "FavoritesMain");
                            fragmentTransaction.addToBackStack(null);
                            active_fragments_history.remove(active_fragments_history.size()-1);
                            active_fragments_history.add("FavoritesMain");
                            fragmentTransaction.commit();
                        }
                        break;
                    }
                    case 5:
                    {
                        if(active_fragments_history.get(active_fragments_history.size()-1).compareTo("FollowRequestList" ) != 0)
                        {
                            drawerLayout.closeDrawer(GravityCompat.START);
                            active_fragments = new FollowRequestList();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentContainer, active_fragments, "FollowRequestList");
                            fragmentTransaction.addToBackStack(null);
                            active_fragments_history.remove(active_fragments_history.size()-1);
                            active_fragments_history.add("FollowRequestList");
                            fragmentTransaction.commit();
                        }
                        break;
                    }
                    case 6:
                    {
                        break;
                    }
                    case 7:
                    {
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
            }
        });

    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //mTextMessage.setText("Home");
                    return true;
                case R.id.navigation_cart:
                    //mTextMessage.setText("Cart");
                    return true;
                case R.id.navigation_filter:
                    //mTextMessage.setText("Filter");
                    return true;
                case R.id.navigation_star:
                    //mTextMessage.setText("Favourities");
                    return true;
            }

            return false;
        }

    };

    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        if(active_fragments_history.size()>0)
        {
            active_fragments_history.remove(active_fragments_history.size()-1);
        }

    }

    @Override
    public void onClickEvent() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
        }
    }

//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        Toast toast = Toast.makeText(this, String.valueOf(id),Toast.LENGTH_LONG);
//        toast.show();
//
//
//
//        return super.onOptionsItemSelected(item);
//    }
//    @Override
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.mert_layout_deneme, menu);
//        return true;
//    }
//
//


}


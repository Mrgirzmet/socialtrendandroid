package com.example.samte.thexprojectandroid;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.samte.thexprojectandroid.Follow_Lists.adapter.UserAdapter;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by samte on 19/12/2017.
 */
class Row
{
     String title;
     String description;
     int[] images;
     public Row(String title,String description,int[] images)
     {
        this.images = images;
        this.description = description;
        this.title = title;
     }
}
public class DrawerLayoutMenuAdapter extends BaseAdapter {
    private ArrayList<Row> myList;
    private Resources res;
    private Context context;
    public float heightOfScreen;

    public DrawerLayoutMenuAdapter(Context _context)
    {
        context=_context;
        myList = new ArrayList<Row>();
        res = context.getResources();
        String [] titles = res.getStringArray(R.array.title);
        String [] descriptions = res.getStringArray(R.array.descriptions);
        int [][] images = new int[titles.length][titles.length];
//        System.out.println(titles[2]);
        images[0][0]=R.drawable.logo_white;
        images[0][1]=R.drawable.back_button_menu;
        images[1][0]=R.drawable.ic_lock_outline_white_48dp;
        images[2][0]=R.drawable.profile;
        images[3][0]=R.drawable.shopping;
        images[4][0]=R.drawable.favorite;
        images[5][0]=R.drawable.requests;

        for (int i = 0; i < titles.length;i++)
        {
            myList.add(new Row(titles[i],descriptions[i],images[i]));
        }
    }
    @Override
    public int getCount() {
        return res.getStringArray(R.array.title).length;
    }

    @Override
    public Object getItem(int i) {
        return myList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.menu_layout_style_1,viewGroup,false);
        FrameLayout logoLayout = (FrameLayout) row.findViewById(R.id.subLayout_Logo);
        ImageView imageLogo = (ImageView) row.findViewById(R.id.imageViewLogo);
        ImageView imageBack = (ImageView) row.findViewById(R.id.imageViewBackButton);

        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickCallback.onClickEvent();
            }
        });
        RelativeLayout subLayout_profileInfo = (RelativeLayout) row.findViewById(R.id.subLayout_profileInfo);
        TextView userNameSurname = (TextView) row.findViewById(R.id.userNameSurname);
        TextView userNickName = (TextView) row.findViewById(R.id.userNickName);
        ImageView imageLock = (ImageView) row.findViewById(R.id.imageLock);

        LinearLayout subLayout_StandardRows = (LinearLayout) row.findViewById(R.id.subLayout_StandardRows);
        ImageView imageProfile = (ImageView) row.findViewById(R.id.imageProfile);
        TextView standardRowText = (TextView) row.findViewById(R.id.standardRowText);

        subLayout_profileInfo.setVisibility(View.GONE);
        subLayout_StandardRows.setVisibility(View.GONE);
        logoLayout.setVisibility(View.GONE);
        System.out.println(getCount());
        if (i == 0)
        {
            logoLayout.setVisibility(View.VISIBLE);
            logoLayout.getLayoutParams().height = (int)(heightOfScreen/10*2.5);
            logoLayout.requestLayout();
            imageLogo.setImageResource(myList.get(i).images[0]);
            imageLogo.setScaleX((float)1.2);
            imageLogo.setScaleY((float)1.2);
            imageBack.setImageResource(myList.get(i).images[1]);
            imageBack.setScaleX((float) 0.4);
            imageBack.setScaleY((float) 0.4);
        }
        else if(i==1)
        {
            subLayout_profileInfo.setVisibility(View.VISIBLE);
            subLayout_profileInfo.getLayoutParams().height = (int)(heightOfScreen/10);
            subLayout_profileInfo.requestLayout();
            userNameSurname.setText(myList.get(i).title);
            userNickName.setText(myList.get(i).description);
            imageLock.setImageResource(myList.get(i).images[0]);
        }
        else if(i < getCount()-2)
        {
            subLayout_StandardRows.setVisibility(View.VISIBLE);
            subLayout_StandardRows.getLayoutParams().height = (int)(heightOfScreen/10);
            subLayout_StandardRows.requestLayout();
            imageProfile.setImageResource(myList.get(i).images[0]);
            standardRowText.setText(myList.get(i).title);
        }
        else
        {
            subLayout_StandardRows.setVisibility(View.VISIBLE);
            subLayout_StandardRows.getLayoutParams().height = (int)(heightOfScreen/10);
            subLayout_StandardRows.requestLayout();
            imageProfile.setVisibility(View.INVISIBLE);
            standardRowText.setText(myList.get(i).title);
        }
        return row;
    }
    public void setItemClickCallback (final ItemClickCallback itemClickCallback){
        this.itemClickCallback = itemClickCallback;
    }
    private ItemClickCallback itemClickCallback;

    public interface ItemClickCallback{
        void onClickEvent();
    }
}

package com.example.samte.thexprojectandroid.Shopping_Cart;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.samte.thexprojectandroid.R;

/**
 * Created by Berkay on 6.01.2018.
 */

public class ShoppingMain extends Fragment{
    private SectionsPageAdapter sectionsPageAdapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.shoppingcart_activity,container,false);
        Log.d("Shopping Cart","onCreate is Starting Yehuuuuu...");

        sectionsPageAdapter = new SectionsPageAdapter(getFragmentManager());
        viewPager = v.findViewById(R.id.container);
        setupViewPager(viewPager);
        tabLayout = v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_shopping_cart_berkay);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_authentication_berkay);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_history_berkay);

        return v;
    }



    public void setupViewPager(ViewPager viewPager){
        SectionsPageAdapter adapter = new SectionsPageAdapter(getFragmentManager());
        adapter.addFragmetn(new ShoppingList());
        adapter.addFragmetn(new ShoppingAuthentication());
        adapter.addFragmetn(new ShoppingHistory());
        viewPager.setAdapter(adapter);
    }



}

package com.example.samte.thexprojectandroid;

import android.animation.Animator;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreen extends AppCompatActivity{

    private ImageView splash_white_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        splash_white_logo = findViewById(R.id.logo_white_splash_screen);

        splash_white_logo.setVisibility(View.VISIBLE);



        splash_white_logo.setAlpha(0f);
        splash_white_logo.animate()
                .alpha(1f)
                .setDuration(1000)
                .setListener(new Animator.AnimatorListener() {
                    private float transparancyTarget = 1f;
                    private int counter = 0;
                    @Override
                    public void onAnimationStart(Animator animation) {
                        System.out.println("Start");
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if(counter < 3){
                            if(transparancyTarget == 1f){
                                transparancyTarget = 0.2f;
                            }else{
                                transparancyTarget = 1f;
                            }
                            splash_white_logo.animate()
                                    .alpha(transparancyTarget)
                                    .setDuration(1000);
                            counter++;
                        }else{
                            Intent intent = new Intent(SplashScreen.this, LoginPage.class);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        System.out.println("Cancel");
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        System.out.println("Repeat");
                    }
                });





    }


}

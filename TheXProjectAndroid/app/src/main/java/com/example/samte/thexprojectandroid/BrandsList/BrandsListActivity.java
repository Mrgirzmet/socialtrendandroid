package com.example.samte.thexprojectandroid.BrandsList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.samte.thexprojectandroid.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Berkay on 22.01.2018.
 */

public class BrandsListActivity extends Fragment {

    private GridView myGrid;
    private DatabaseReference mDatabaseRef;
    private ImageView imageView;
    private List<ImageUpload> brandsList;
    private BrandsListAdapter adapter;
    private ProgressDialog progressDialog;
    MaterialSearchBar searchBar;

    public static final String DATABASE_PATH = "image";


    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.brandslist_activity,container,false);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait while loading...");
        progressDialog.show();

        imageView = v.findViewById(R.id.imageViewBrandsList);
        myGrid = v.findViewById(R.id.gridView);
        brandsList = new ArrayList<>();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(DATABASE_PATH);
        searchBar = v.findViewById(R.id.searchBarBrandsList);

        searchBar.setSpeechMode(false);
        searchBar.setHint("Search...");

        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressDialog.dismiss();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    ImageUpload img = snapshot.getValue(ImageUpload.class);
                    brandsList.add(img);
                }

                adapter = new BrandsListAdapter(getActivity(),R.layout.brandslist_singleitem,brandsList);
                myGrid.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });

        searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        myGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getActivity(),adapter.getItem(i).toString(),Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }


    class BrandsListAdapter extends ArrayAdapter<ImageUpload>{
        private Activity context;
        private int resource;
        private List<ImageUpload> listImage;
        String nameOfBrand;

        public BrandsListAdapter(@NonNull Activity context, int resource, @NonNull List<ImageUpload> objects) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
            listImage = objects;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            View v = layoutInflater.inflate(resource,null);
            ImageView img = v.findViewById(R.id.imageViewBrandsList);

            nameOfBrand = listImage.get(position).getName();
            Glide.with(context).load(listImage.get(position).getUrl()).into(img);

            return v;
        }
    }

}

package com.example.samte.thexprojectandroid.Brands1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.samte.thexprojectandroid.R;

/**
 * Created by Berkay on 5.01.2018.
 */

public class Brands1 extends Fragment {
    ViewPager viewPager;
    Brands1Adapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.brands1_activity,container,false);
        viewPager = v.findViewById(R.id.brand1pager);
        adapter = new Brands1Adapter(getContext());
        viewPager.setAdapter(adapter);
        return v;
    }
}

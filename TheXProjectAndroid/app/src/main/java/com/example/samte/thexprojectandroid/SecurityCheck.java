package com.example.samte.thexprojectandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SecurityCheck extends AppCompatActivity {

    private String code;
    private EditText txtCode;
    private Button btnChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_check);

        Bundle bundle = getIntent().getExtras();
        code = bundle.getString("code");

        txtCode = findViewById(R.id.txt_code_sc);
        btnChange = findViewById(R.id.btn_send_sc);

        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkCode()){
                    //Other Page
                }
            }
        });

    }

    private boolean checkCode(){
        if(txtCode.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Please enter your code!", Toast.LENGTH_LONG).show();
            return true;
        }else if(!txtCode.getText().toString().equals("")){
            if(txtCode.getText().toString().equals(code)){
                Toast.makeText(getApplicationContext(), "Succesful Apply", Toast.LENGTH_SHORT).show();
                return true;
            }else{
                Toast.makeText(getApplicationContext(), "Wrong Code Apply!", Toast.LENGTH_SHORT).show();
                return false;
            }

        }else{
            return false;
        }
    }
}

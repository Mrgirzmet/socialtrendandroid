package com.example.samte.thexprojectandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samte.thexprojectandroid.Favorites.FavoritesMain;
import com.example.samte.thexprojectandroid.FlowActivity.FlowFragmentActivity;
import com.example.samte.thexprojectandroid.FlowActivity.FlowPage;
import com.example.samte.thexprojectandroid.FlowActivity.UserInfo;
import com.example.samte.thexprojectandroid.Follow_Lists.ui.FollowRequestList;
import com.example.samte.thexprojectandroid.ProfileFragmentActivity.ProfileFragment;
import com.example.samte.thexprojectandroid.Shopping_Cart.ShoppingMain;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import java.util.Stack;

public class MainActivity extends AppCompatActivity implements FlowPage.FollowPageCommunicator {
    ImageButton logoButton;
    ImageButton backButton;
    ImageButton lockButton;
    ImageButton profileButton;
    TextView profileTextView;
    ImageButton shoppingCartButton;
    TextView shoppingTextView;
    ImageButton favoritesButton;
    TextView favoritesTextView;
    ImageButton requestsButton;
    TextView requestsTextView;
    TextView settingsAndPrivacy;
    TextView helpCenter;
    //added by omer
    TextView logout;
    FirebaseAuth firebaseAuth;
    TextView usernamesurname;
    TextView usernickname;
    Stack<Fragment> active_fragments;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    BottomNavigationView navigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //added by omer
        firebaseAuth = FirebaseAuth.getInstance();
        /*if(firebaseAuth.getCurrentUser() == null){
            finish();
            startActivity(new Intent(this, LoginPage.class));
        }*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navigation = findViewById(R.id.toolbar_bottom);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        try
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }catch (NullPointerException ex)
        {
            Toast.makeText(this,ex.toString(),Toast.LENGTH_SHORT).show();
        }
        DrawerLayout drawer;
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        logoButton =  navigationView.findViewById(R.id.logoButton);
        logoButton.setOnClickListener(drawerMenuOnClickListener);
        backButton = navigationView.findViewById(R.id.menuBackButton);
        backButton.setOnClickListener(drawerMenuOnClickListener);
        lockButton = navigationView.findViewById(R.id.lockButton);
        lockButton.setOnClickListener(drawerMenuOnClickListener);

        profileButton = navigationView.findViewById(R.id.profile_button);
        profileButton.setOnClickListener(drawerMenuOnClickListener);
        profileTextView = navigationView.findViewById(R.id.profileTextView);
        profileTextView.setOnClickListener(drawerMenuOnClickListener);

        shoppingCartButton =  navigationView.findViewById(R.id.shoppingCartButton);
        shoppingCartButton.setOnClickListener(drawerMenuOnClickListener);
        shoppingTextView = navigationView.findViewById(R.id.shoppingCartTextView);
        shoppingTextView.setOnClickListener(drawerMenuOnClickListener);

        favoritesButton = navigationView.findViewById(R.id.favoritesButton);
        favoritesButton.setOnClickListener(drawerMenuOnClickListener);
        favoritesTextView = navigationView.findViewById(R.id.favoritesTextView);
        favoritesTextView.setOnClickListener(drawerMenuOnClickListener);

        requestsButton = navigationView.findViewById(R.id.requestsButton);
        requestsButton.setOnClickListener(drawerMenuOnClickListener);
        requestsTextView = navigationView.findViewById(R.id.requestsTextView);
        requestsTextView.setOnClickListener(drawerMenuOnClickListener);

        settingsAndPrivacy =  navigationView.findViewById(R.id.settingsAndPrivacyTextView);
        settingsAndPrivacy.setOnClickListener(drawerMenuOnClickListener);
        helpCenter =  navigationView.findViewById(R.id.helpCenterTextView);
        helpCenter.setOnClickListener(drawerMenuOnClickListener);

        //added by omer
        logout = navigationView.findViewById(R.id.txt_logout);
        logout.setOnClickListener(drawerMenuOnClickListener);
        FirebaseUser user = firebaseAuth.getCurrentUser();
        usernamesurname = navigationView.findViewById(R.id.userNameSurname);
        usernickname = navigationView.findViewById(R.id.userNickName);
        usernamesurname.setText(user.getDisplayName());
        usernickname.setText(user.getEmail());
        active_fragments = new Stack<Fragment>();
        active_fragments.push(new FlowPage());
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer,active_fragments.peek(),"FlowPage");
        fragmentTransaction.addToBackStack("FlowPage");
        fragmentTransaction.commit();
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if (active_fragments.peek().getTag().compareTo("FlowPage") != 0 )
                    {
                        getSupportActionBar().show();
                        navigation.setVisibility(View.VISIBLE);
                        active_fragments.push(new FlowPage());
                        fragmentManager = getSupportFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fragmentContainer,active_fragments.peek(),"FlowPage");
                        fragmentTransaction.addToBackStack("FlowPage");
                        fragmentTransaction.commit();
                    }
                    return true;
                case R.id.navigation_cart:
                    if(active_fragments.peek().getTag().compareTo("ShoppingMain" ) != 0) {
                        active_fragments.push(new ShoppingMain());
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fragmentContainer, active_fragments.peek(), "ShoppingMain");
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }
                    return true;
                case R.id.navigation_filter:
//                    Toast.makeText(getBaseContext(),"navigation_filter",Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.navigation_star:
//                    Toast.makeText(getBaseContext(),"navigation_star",Toast.LENGTH_SHORT).show();
                    if(active_fragments.peek().getTag().compareTo("Favorites" ) != 0) {
                        active_fragments.push(new FavoritesMain());
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fragmentContainer, active_fragments.peek(), "Favorites");
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        if(active_fragments.size() > 1)
        {
            if (active_fragments.peek().getTag().compareTo("FlowPage") == 0 )
            {
                getSupportActionBar().show();
                navigation.setVisibility(View.VISIBLE);
            }
            active_fragments.pop();
            if(active_fragments.peek().getTag().compareTo("FlowPage")==0)
            {
                navigation.setSelectedItemId(R.id.navigation_home);
            }else if(active_fragments.peek().getTag().compareTo("Favorites")==0)
            {
                navigation.setSelectedItemId(R.id.navigation_star);
            }else if(active_fragments.peek().getTag().compareTo("ShoppingMain")==0)
            {
                navigation.setSelectedItemId(R.id.navigation_cart);
            }
            DrawerLayout drawer =  findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    View.OnClickListener drawerMenuOnClickListener;
    {
        drawerMenuOnClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                int id = v.getId();
                if (id == R.id.logoButton) {
                    Toast.makeText(getBaseContext(),"Logo Button has been pressed",Toast.LENGTH_SHORT).show();
                } else if (id == R.id.menuBackButton) {
//                    Toast.makeText(getBaseContext(),"menuBack Button has been pressed",Toast.LENGTH_SHORT).show();
                } else if ((id == R.id.profile_button)  || (id == R.id.profileTextView)) {
//                    Toast.makeText(getBaseContext(),"profile Button has been pressed",Toast.LENGTH_SHORT).show();
                    if(active_fragments.peek().getTag().compareTo("Profile" ) != 0) {
                        active_fragments.push(new ProfileFragment());
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fragmentContainer, active_fragments.peek(), "Profile");
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
//                        navigation.setSelectedItemId(R.id.navigation_cart);
                    }
                } else if ((id == R.id.shoppingCartButton) || (id == R.id.shoppingCartTextView)) {
                    if(active_fragments.peek().getTag().compareTo("ShoppingMain" ) != 0) {
                        active_fragments.push(new ShoppingMain());
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fragmentContainer, active_fragments.peek(), "ShoppingMain");
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        navigation.setSelectedItemId(R.id.navigation_cart);
                    }
                } else if ((id == R.id.favoritesButton)  || (id == R.id.favoritesTextView)) {
                    if(active_fragments.peek().getTag().compareTo("Favorites" ) != 0) {
                        active_fragments.push(new FavoritesMain());
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fragmentContainer, active_fragments.peek(), "Favorites");
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        navigation.setSelectedItemId(R.id.navigation_star);
                    }
                } else if ((id == R.id.requestsButton)  || (id == R.id.requestsTextView)) {
                    if(active_fragments.peek().getTag().compareTo("FallowRequest" ) != 0) {
                        active_fragments.push(new FollowRequestList());
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fragmentContainer, active_fragments.peek(), "FallowRequest");
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }
                }
                else if(id == R.id.lockButton) {
//                    Toast.makeText(getBaseContext(),"lock Button has been pressed",Toast.LENGTH_SHORT).show();
                }
                else if(id == R.id.settingsAndPrivacyTextView) {
//                    Toast.makeText(getBaseContext(),"settingsAndPrivacy text has been pressed",Toast.LENGTH_SHORT).show();
                }
                else if(id == R.id.helpCenterTextView) {
//                    Toast.makeText(getBaseContext(),"helpCenter text has been pressed",Toast.LENGTH_SHORT).show();
                }else if(id == R.id.txt_logout){
                    firebaseAuth.signOut();
                    finish();
                    startActivity(new Intent(getApplicationContext(), LoginPage.class));
                }
                drawer.closeDrawer(GravityCompat.START);
            }
        };
    }
    @Override
    public void requestingFullScreen(UserInfo userInfo) {
        getSupportActionBar().hide();
        navigation.setVisibility(View.GONE);
        Toast.makeText(getBaseContext(),"clickedCallback",Toast.LENGTH_SHORT).show();
        Bundle bundle = new Bundle();
        bundle.putString("username",userInfo.getUsername());
        bundle.putInt("productImage",userInfo.getProductResourceInformation());
        active_fragments.push(new FlowFragmentActivity());
        active_fragments.peek().setArguments(bundle);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer, active_fragments.peek(),"FlowPage");
        fragmentTransaction.addToBackStack("FlowPage");
        fragmentTransaction.commit();
    }
}
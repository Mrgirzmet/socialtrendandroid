package com.example.samte.thexprojectandroid.FlowActivity;

/**
 * Created by samte on 22/01/2018.
 */

public class UserInfo {
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public Integer getProductResourceInformation() {
        return productResourceInformation;
    }
    public void setProductResourceInformation(Integer productResourceInformation) {
        this.productResourceInformation = productResourceInformation;
    }
    public UserInfo(String username, Integer productResourceInformation) {

        this.username = username;
        this.productResourceInformation = productResourceInformation;
    }
    private String username;
    private Integer productResourceInformation;
}

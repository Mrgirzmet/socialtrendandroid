package com.example.samte.thexprojectandroid.Profile;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.example.samte.thexprojectandroid.R;

import java.lang.reflect.Array;
import java.util.List;

/**
 * Created by C505 on 9.01.2018.
 */

public class ProfileAdapter extends ArrayAdapter<Product>{

    public ProfileAdapter(@NonNull Context context, int resource, @NonNull List<Product> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if (v == null){
            LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = layoutInflater.inflate(R.layout.brandslist_singleitem, null);
        }

        Product product = getItem(position);
        ImageView imageView = (ImageView)v.findViewById(R.id.imageViewBrandsList);

        return v;
    }
}

package com.example.samte.thexprojectandroid;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class CreatingAccount extends AppCompatActivity {

    private Button btnCreateAccount;
    private EditText txtUsername;
    private EditText txtPhoneNumber;
    private EditText txtEmail;
    private EditText txtPassword;

    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creating_account);

        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);

        btnCreateAccount = findViewById(R.id.btn_create_account);
        txtUsername = findViewById(R.id.txt_username_ca);
        txtPhoneNumber = findViewById(R.id.txt_phonenumber_ca);
        txtEmail = findViewById(R.id.txt_email_ca);
        txtPassword = findViewById(R.id.txt_password_ca);

        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAccount();
            }
        });


    }

    private void createAccount(){
        final String username = txtUsername.getText().toString().trim();
        String phonenumber = txtPhoneNumber.getText().toString().trim();
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();


        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            txtEmail.setError(getString(R.string.error_field_required));
            focusView = txtEmail;
            cancel = true;
        } else if (!isEmailValid(email)) {
            txtEmail.setError(getString(R.string.error_invalid_email));
            focusView = txtEmail;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            txtPassword.setError(getString(R.string.error_invalid_password));
            focusView = txtPassword;
            cancel = true;
        }

        if(!TextUtils.isEmpty(username) && !isUsernameValid(username)){
            txtUsername.setError(getString(R.string.error_invalid_username));
            focusView = txtUsername;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            progressDialog.setMessage("Creating account...");
            progressDialog.show();



            firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                FirebaseUser user = firebaseAuth.getCurrentUser();
                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(username).build();
                                user.updateProfile(profileUpdates);
                                finish();
                                startActivity(new Intent(getApplicationContext(), LoginPage.class));
                                Toast.makeText(CreatingAccount.this, "Created Succesfully", Toast.LENGTH_SHORT).show();

                            }else {
                                Toast.makeText(CreatingAccount.this, "Could not created, please try again", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

        }



    }

    private boolean isEmailValid(String email) {
        return (email.contains("@") && email.contains("."));
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 8;
    }

    private boolean isUsernameValid(String username) {
        return username.length() > 8;
    }
}
